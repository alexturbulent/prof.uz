import React from 'react';
import styled from "styled-components";
import Image from "next/image";
import { Row, Col } from "react-bootstrap";
import PrimaryBtn from "../commonComponents/PrimaryBtn";
import Breadcrumb from "../commonComponents/Breadcrumb";
import { InfoCircleFill } from 'react-bootstrap-icons';

const Content = styled.div`
    margin: 60px auto;
`

const Description = styled.h4`
    margin: 20px 0;
    color: #1C1C1C;
`

const Alert = styled.div`
    background: #FFEDD9;
    border-radius: 8px;
    padding: 20px;
    margin-bottom: 20px;

    .heading {
        font-weight: bold;
        color: #EE8100;
        display: flex;
        align-items: center;
        margin-bottom: 5px;
        
        span {
            margin-left: 7px;
        }
    }

    .text {
        color: #1C1C1C;
    }
`

const ImageCol = styled(Col)`
    text-align: center;
`

const breadcrumbList = [
    {
        path: '/',
        label: 'Asosiy'
    },
    {
        path: '/start-test',
        label: 'Test'
    },
]

const Professions = () => {
    return (
        <>
            <Breadcrumb list={breadcrumbList} />

            <Content className="container">
                <Row>
                    <Col md={12} lg={6}>
                        <h2 className="section-title">
                            Test 20 ta savoldan iborat
                        </h2>

                        <Description>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                        </Description>

                        <Alert>
                            <Row>
                                <Col className="heading" xs={12}>
                                    <InfoCircleFill /> <span>Eslatma</span>
                                </Col>

                                <Col className="text" xs={12}>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </Col>
                            </Row>
                        </Alert>

                        <PrimaryBtn>
                            Testni boshla
                        </PrimaryBtn>
                    </Col>

                    <ImageCol md={12} lg={6}>
                        <Image
                            src="/images/start_test.png"
                            width={460}
                            height={470}
                            alt="Error"
                        />
                    </ImageCol>
                </Row>
            </Content>
        </>
    );
}

export default Professions;