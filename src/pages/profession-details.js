import Breadcrumb from "../commonComponents/Breadcrumb";
import Jumbotron from "./professionDetails/Jumbotron";
import Types from "./professionDetails/Types";
import Abilities from "./professionDetails/Abilities";

const breadcrumbList = [
    {
        path: '/',
        label: 'Asosiy'
    },
    {
        path: '/professions',
        label: 'Professions'
    },
    {
        path: '/profession-details',
        label: 'Profession Details'
    },
]

const ProfessionDetails = () => {
    return (
        <>
            <Breadcrumb list={breadcrumbList} />
            <Jumbotron />
            <Types />
            <Abilities />
        </>
    );
}

export default ProfessionDetails;