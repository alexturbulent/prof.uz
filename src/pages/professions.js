import React from 'react';
import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Image from "next/image";
import Select from "../commonComponents/Select";
import SmallPrimaryBtn from "../commonComponents/SmallPrimaryBtn";
import PageHeading from "../commonComponents/PageHeading";


const FormRow = styled.div`
    margin: 40px auto;
`

const Content = styled.div`
    margin: 20px auto;
`

const Card = styled.div`
    background: #F7F7F7;
    border: 1px solid #E7E7E7;
    border-radius: 10px;
    padding: 15px;
    margin-bottom: 20px;
`

const CardTitle = styled.h4`
    font-family: Nunito;
    font-weight: bold;
    font-size: 16px;
    line-height: 22px;
    letter-spacing: -0.1px;
    color: #000000;
`

const CardText = styled.h4`
    font-size: 12px;
    line-height: 15px;
    color: #6E7381;
`

const list = [
    {
        id: 1,
        title: 'Shifokor',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 2,
        title: 'O’qituvchi',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 3,
        title: 'Injener muhandis',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 4,
        title: 'Shifokor',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 5,
        title: 'O’qituvchi',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 6,
        title: 'Injener muhandis',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 7,
        title: 'Shifokor',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 8,
        title: 'O’qituvchi',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 9,
        title: 'Injener muhandis',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 10,
        title: 'Shifokor',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 11,
        title: 'O’qituvchi',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
    {
        id: 12,
        title: 'Injener muhandis',
        work: 'Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari',
        salary: 'Maoshi*: 1 500 000 - 3 000 000 sum',
        img: '/images/slide_1.png',
    },
]

const Professions = () => {
    return (
        <>
            <PageHeading>
                Barcha kasblar
            </PageHeading>

            <FormRow className="container">
                <Form>
                    <Row>
                        <Col lg={6}>
                            <Form.Group>
                                <Select placeholder="Soha bo’yicha tanlash">
                                    <option value="DICTUM">Dictamen</option>
                                    <option value="CONSTANCY">Constancia</option>
                                    <option value="COMPLEMENT">Complemento</option>
                                </Select>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Select>
                                    <option value="DICTUM">Dictamen</option>
                                    <option value="CONSTANCY">Constancia</option>
                                    <option value="COMPLEMENT">Complemento</option>
                                </Select>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Select>
                                    <option value="DICTUM">Dictamen</option>
                                    <option value="CONSTANCY">Constancia</option>
                                    <option value="COMPLEMENT">Complemento</option>
                                </Select>
                            </Form.Group>
                        </Col>
                    </Row>
                </Form>
            </FormRow>

            <Content className="container">
                <Row>
                    {
                        list.map((item) => (
                            <Col md={6} lg={4} key={item.id}>
                                <Card>
                                    <Row>
                                        <Col lg={4}>
                                            <Image
                                                src={item.img}
                                                alt={item.title}
                                                height={150}
                                                width={100}
                                            />
                                        </Col>
                                        <Col>
                                            <CardTitle>
                                                {item.title}
                                            </CardTitle>
                                            <CardText>
                                                {item.work}
                                            </CardText>
                                            <CardText>
                                                {item.salary}
                                            </CardText>

                                            <SmallPrimaryBtn>
                                                Batafsil
                                            </SmallPrimaryBtn>
                                        </Col>
                                    </Row>
                                </Card>
                            </Col>
                        ))
                    }
                </Row>
            </Content>
        </>
    );
}

export default Professions;