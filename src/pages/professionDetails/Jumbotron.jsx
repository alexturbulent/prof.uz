import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import { Search, Bank } from "react-bootstrap-icons";

const Jumbotron = styled.div`
    background-image: url('/images/profession_details.png');
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 600px;
    position: relative;
    margin-bottom: 100px;
`

const StyledRow = styled(Row)`
    margin: 0 auto;
    padding-top: 40px;

    &:first-child {
        padding-left: 0;
    }

    .section-title {
        margin-bottom: 10px;
    }
`

const OutBox = styled(Row)`
    width: 500px;
    background: #FFFFFF;
    border: 1px solid #E7E7E7;
    box-shadow: 0px 2px 10px rgba(75, 90, 104, 0.1);
    border-radius: 16px;
    padding: 40px;
    position: absolute;
    bottom: -70px;

    .outbox-row {
        display: flex;
        align-items: center;
    }

    h4 {
        color: #1C1C1C;
        margin-bottom: 5px;
    }

    p {
        color: #6E7381;
    }
`

const IconBox = styled.div`
    background: #FFA945;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    width: 72px;
    height: 72px;

    .styled-icon {
        color: #FFFFFF;
        font-size: 32px;
    }
`

const JumbotronComponent = () => {
    return (
        <Jumbotron>
            <StyledRow className="container">
                <Col lg={6}>
                    <h2 className="section-title">
                        Shifokor kaspi haqida
                    </h2>

                    <p className="description">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.
                    </p>
                </Col>
            </StyledRow>

            <div className="container">
                <OutBox>
                    <Col xs={12} className="mb-3">
                        <Row className="outbox-row">
                            <Col xs={3}>
                                <IconBox>
                                    <Search className="styled-icon" />
                                </IconBox>
                            </Col>
                            <Col>
                                <h4>
                                    Hozirgi talab
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore  aliqua.
                                </p>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs={12} className="mb-3">
                        <Row className="outbox-row">
                            <Col xs={3}>
                                <IconBox>
                                    <Bank className="styled-icon" />
                                </IconBox>
                            </Col>
                            <Col>
                                <h4>
                                    Qayerlarda ishlaydi?
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore  aliqua.
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </OutBox>
            </div>
        </Jumbotron>
    );
}

export default JumbotronComponent;