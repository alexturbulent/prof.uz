import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import Image from "next/image";

const Container = styled.div`
    height: 210px;
    background: #FFA31A;
    border-radius: 16px;
    padding: 40px;
    margin-bottom: 140px;

    h2 {
        color: #ffffff;
        margin-bottom: 30px;
    }
`

const Card = styled.div`
    background: #FFFFFF;
    box-shadow: 0px 2px 10px rgba(75, 90, 104, 0.1);
    border-radius: 10px;
    padding: 6px;
    height: 180px;
    overflow: hidden;
    text-align: center;
    
    .content {
        text-align: left;
        padding: 0 10px;
    }

    h4 {
        margin-top: 12px;
        font-family: Nunito;
        font-weight: bold;
        line-height: 22px;
    }

    p {
        font-size: 12px;
        line-height: 15px;
        color: #6E7381;
    }
`

const list = [
    {
        id: 1,
        title: "Stomatolog",
        subtitle: "Tish davolovchi shifokor",
        img: "/images/profession_details_slide.png",
    },
    {
        id: 2,
        title: "Xirurg",
        subtitle: "Umumiy amaliyot shifokori",
        img: "/images/profession_details_slide_2.png",
    },
    {
        id: 777,
        title: "Akusher",
        subtitle: "Ayollar va tug’ruq",
        img: "/images/profession_details_slide_3.png",
    },
    {
        id: 4,
        title: "Stomatolog",
        subtitle: "Tish davolovchi shifokor",
        img: "/images/profession_details_slide.png",
    },
    {
        id: 5,
        title: "Xirurg",
        subtitle: "Umumiy amaliyot shifokori",
        img: "/images/profession_details_slide_2.png",
    },
    {
        id: 6,
        title: "Akusher",
        subtitle: "Ayollar va tug’ruq",
        img: "/images/profession_details_slide_3.png",
    },
]

const Types = () => {
    return (
        <Container className="container">
            <h2 className="section-title">
                Shifokor turlari
            </h2>

            <Row className="card-row gx-1">
                {
                    list.map((item) => (
                        <Col key={item.id} className="pl-2 pr-2">
                            <Card>
                                <Image
                                    src={item.img}
                                    width={150}
                                    height={100}
                                    alt="Error"
                                />
                                <div className="content">
                                    <h4>
                                        {item.title}
                                    </h4>

                                    <p>
                                        {item.subtitle}
                                    </p>
                                </div>
                            </Card>
                        </Col>
                    ))
                }
            </Row>
        </Container>
    );
}

export default Types;