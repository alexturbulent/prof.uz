import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import Image from "next/image";
import { Check2 } from "react-bootstrap-icons";
import Label from "../../commonComponents/Label";

const Description = styled.p`
    color: #6E7381;
    margin-bottom: 20px;
`

const ListRow = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 15px;
    
    &:last-child {
        margin-bottom: 0;
    }
`

const IconBox = styled.div`
    background: #CFFFCE;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    width: 36px;
    height: 36px;

    .styled-icon {
        color: #FFFFFF;
        font-size: 32px;
    }
`

const list = [
    "Insonlarga mehribonlik g’amxo’rlik",
    "Yangiliklarga bo’lgan qiziqish va o’rganish",
    "Biologiya va anatomiya fanlariga qiziqish",
    "Yangiliklarga bo’lgan qiziqish va o’rganish",
    "Qo’rqmaslik va jiddiylik o’zini tutish",
]

const Abilities = () => {

    return (
        <div className="container">

            <Row>
                <Col md={12} lg={6} className="text-center">
                    <Image
                        src="/images/start_test.png"
                        width={460}
                        height={470}
                        alt="Error"
                    />
                </Col>

                <Col>
                    <Label>
                        Shifokor uchun
                    </Label>

                    <h3 className="section-title mb-2">
                        Shaxsiy qobiliyatlar
                    </h3>

                    <Description>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    </Description>

                    {
                        list.map((item, index) => (
                            <ListRow key={index}>
                                <Col xs={1} className="pl-0">
                                    <IconBox>
                                        <Check2 />
                                    </IconBox>
                                </Col>
                                <Col>
                                    {item}
                                </Col>
                            </ListRow>
                        ))
                    }
                </Col>
            </Row>
        </div>
    );
}

export default Abilities;