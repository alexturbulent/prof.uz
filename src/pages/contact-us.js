import styled from "styled-components";
import { Row, Col } from "react-bootstrap";
import Image from "next/image";
import { Telephone, Envelope, GeoAlt } from "react-bootstrap-icons";
import PageHeading from "../commonComponents/PageHeading";

const Content = styled.div`
    padding: 40px 0 0;
    
    .map {
        overflow: hidden;
        margin: 50px 0;

        p {
            margin-bottom: 15px;
            font-size: 12px;
            line-height: 15px;
            text-align: center;
            color: #6E7381;
        }

        iframe {
            position: relative;
            border: none;
        }
    }
`

const IconBox = styled.div`
    background: #48CCD5;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    width: 48px;
    height: 48px;
    margin: 0 auto 15px;

    .styled-icon {
        color: #FFFFFF;
        font-size: 18px;
    }
`

const ContactUs = () => {
    return (
        <>
            <PageHeading>
                Bog&apos;lanish
            </PageHeading>

            <Content className="small-container">
                <Row>
                    <Col className="text-center">
                        <IconBox>
                            <Telephone className="styled-icon" />
                        </IconBox>
                        <p>
                            +998 97 1234567 <br />
                            +998 97 1234567 <br />
                        </p>
                    </Col>
                    <Col className="text-center">
                        <IconBox>
                            <Envelope className="styled-icon" />
                        </IconBox>
                        <p>
                            info@xtv.uz <br />
                            info@websie.com<br />
                        </p>
                    </Col>
                    <Col className="text-center">
                        <IconBox>
                            <GeoAlt className="styled-icon" />
                        </IconBox>
                        <p>
                            Tashkent, A Qodiriy ko’chasi, 123 bino,
                        </p>
                    </Col>
                </Row>

                <div className="map">
                    <p>
                        Ish vaqtimiz: Dushanba - Juma, 9:00 - 18:00
                    </p>

                    <iframe
                        src="https://yandex.uz/map-widget/v1/-/CCUefZukCA"
                        width="620"
                        height="400"
                    >
                    </iframe>
                </div>
            </Content>
        </>
    );
}

export default ContactUs;