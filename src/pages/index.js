import Head from "next/head";
import Home from "./Home/Home";

export default function Main() {
    return (
        <div>
            <Head>
                <title>Prof.uz</title>
                <meta name="description" content="Prof.uz" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Home />
        </div>
    )
}
