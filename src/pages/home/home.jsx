import React from 'react';

import Jumbotron from "./components/Jumbotron";
import About from "./components/About";
import Featured from "./components/Featured";
import Steps from "./components/Steps";
import Profession from "./components/Profession";
import ProfessionSlider from "./components/ProfessionSlider";

const Home = () => {
    return (
        <>
            <Jumbotron />
            <About />
            <Featured />
            <Steps />
            <Profession />
            <ProfessionSlider />
        </>
    );
}

export default Home;