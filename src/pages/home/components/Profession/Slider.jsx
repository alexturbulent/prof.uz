import styled from "styled-components";
import { Swiper, SwiperSlide } from "swiper/react";
import { Row, Col } from "react-bootstrap";
import Link from "next/link";
import SwiperCore, { Navigation, Pagination } from "swiper/core";
import SmallPrimaryBtn from "../../../../commonComponents/SmallPrimaryBtn";

import "swiper/swiper.min.css";
import "swiper/components/navigation/navigation.min.css";


SwiperCore.use([Navigation, Pagination]);

const CarouselStyled = styled(Swiper)`
    margin-bottom: 30px;
    text-align: left;

    a {
        &:hover {
            color: #6E7381;
            text-decoration: none;
        }
    }
`

const CarouselItem = styled(Row)`
    background: #FFFFFF;
    border-radius: 10px;
    padding: 10px;
    margin: 0;

    img {
        width: 100%;
    }

    h4 {
        color: #000000;
        font-family: Nunito;
        font-weight: bold;
    }

    p {
        font-size: 12px;
        line-height: 15px;
        color: #6E7381;
    }

    button {
        margin-top: 15px;
    }
`

const list = [
    {
        id: 1,
        title: "Shifokor",
        work: "Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari",
        salary: "Maoshi*: 1 500 000 - 3 000 000 sum",
        img: '/images/slide_1.png',
    },
    {
        id: 2,
        title: "O’qituvchi",
        work: "Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari",
        salary: "Maoshi*: 1 500 000 - 3 000 000 sum",
        img: '/images/slide_2.png',
    },
    {
        id: 3,
        title: "Arxitektor",
        work: "Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari",
        salary: "Maoshi*: 1 500 000 - 3 000 000 sum",
        img: '/images/slide_3.png',
    },
    {
        id: 4,
        title: "Usta",
        work: "Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari",
        salary: "Maoshi*: 1 500 000 - 3 000 000 sum",
        img: '/images/slide_1.png',
    },
    {
        id: 5,
        title: "Haydovchi",
        work: "Ish joyi: Maktab, Universitet, Kollej va boshqa o’quv markazlari",
        salary: "Maoshi*: 1 500 000 - 3 000 000 sum",
        img: '/images/slide_2.png',
    },
]

export default function App() {
    return (
        <>
            <CarouselStyled
                navigation={true}
                slidesPerView={3}
                spaceBetween={30}
                loop={true}
                pagination={{
                    clickable: true
                }}
            >
                {
                    list.map((item) => (
                        <SwiperSlide key={item.id}>
                            <Link href="/profession-details">
                                <a>
                                    <CarouselItem>
                                        <Col xs={4} className="p-0">
                                            <img src={item.img} alt="Error" />
                                        </Col>
                                        <Col>
                                            <h4>
                                                {item.title}
                                            </h4>
                                            <p>
                                                {item.work}
                                            </p>
                                            <p>
                                                {item.salary}
                                            </p>

                                            <SmallPrimaryBtn>
                                                Batafsil
                                            </SmallPrimaryBtn>
                                        </Col>
                                    </CarouselItem>
                                </a>
                            </Link>
                        </SwiperSlide>
                    ))
                }
            </CarouselStyled>
        </>
    );
}
