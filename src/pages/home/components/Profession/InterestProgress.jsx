import { ProgressBar } from "react-bootstrap";
import styled from "styled-components";

const ProgressRow = styled.div`
    margin-bottom: 20px;

    p {
        font-size: 12px;
        line-height: 15px;
        color: #1C1C1C;
        margin-bottom: 5px;
    }
`

const Progress = styled(ProgressBar)`
    margin-bottom: 20px;
    border-radius: 10px;
    height: 8px;
    background-color: #D6D6D6;

    .progress-bar {
        background-color: #FFA945;
    }
`

const InterestProgress = (interest = {}) => {
    return (
        <ProgressRow>
            <p>
                {interest.label}
            </p>
            <Progress now={interest.percent} />
        </ProgressRow>
    );
}

export default InterestProgress;