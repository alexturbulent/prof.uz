import React from 'react';
import { Radar } from 'react-chartjs-2';

const data = {
    labels: [
        'Odamlar bilan muloqot',
        'Fikrlash, tahlil va analiz',
        'Ijodkorlik va ijodiy fikrlash',
        'Muammoga yechim topish',
        'Aniq fanlar va hisob-kitob',
        'Jismoniy quvvat'
    ],
    datasets: [
        {
            label: ' qobilyatlar',
            data: [2, 9, 3, 5, 2, 3],
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1,
        },
    ],
};

const options = {
    scale: {
        ticks: { beginAtZero: true },
    },
};

const RadarChart = () => <Radar data={data} options={options} />

export default RadarChart;