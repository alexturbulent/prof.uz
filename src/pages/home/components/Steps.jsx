
import React from 'react';
import styled from 'styled-components';

const Heading = styled.h3`
    text-align: center;
    color: #1C1C1C;
    margin-bottom: 3px;
`

const Subheading = styled.p`
    text-align: center;
    color: #6E7381;
    width: 70%;
    margin: auto;
    margin-bottom: 30px;
`

const Steps = () => {
    return (
        <div className="container">
            <Heading className="section-title">
                Qanday qilib aniqlaymiz?
            </Heading>

            <Subheading>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
            </Subheading>
        </div>
    );
}

export default Steps;