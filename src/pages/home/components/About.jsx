import React from 'react';
import Image from "next/image";
import { Row, Col } from "react-bootstrap";
import styled from 'styled-components';
import Label from "../../../commonComponents/Label";

const AboutBox = styled.div`
    padding: 30px 0;
`

const StyledRow = styled(Row)`
    display: flex;
    align-items: center;
`

const TextBox = styled.div`
    width: 170px;
    padding: 12px 18px;
    background: #48CCD5;
    border-radius: 16px;
    position: absolute;
    top: 70px;
    right: 10px;
    color: #FFFFFF;
`

const ContentCol = styled(Col)`
    padding-left: 20px;

    @media (max-width: 992px) {
        padding: 50px 0;
    }
`

const Title = styled.h3`
    color: #1C1C1C;
    margin-bottom: 10px;
`

const Content = styled.p`
    color: #6E7381;
`

const About = () => {
    return (
        <AboutBox className="container">
            <StyledRow>
                <Col md={12} lg={6}>
                    <Image
                        src='/images/home_about.png'
                        width={520}
                        height={590}
                        alt="Img error"
                    />

                    <TextBox>
                        Mening kelajagim o’z qo’limda
                    </TextBox>
                </Col>

                <ContentCol md={12} lg={6}>
                    <Label>
                        Loyiha haqida
                    </Label>

                    <Title className="section-title">
                        Qobiliyat va imkoniyatlaringiz darajasini aniqlang
                    </Title>

                    <Content>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    </Content>
                </ContentCol>
            </StyledRow>
        </AboutBox>
    );
}

export default About;