import React from 'react';
import { Row, Col } from "react-bootstrap";
import styled from 'styled-components';
import InterestProgress from "./Profession/InterestProgress";
import Chart from "./Profession/Chart";
import { interests } from "../../../constants";

const Container = styled.div`
    margin-bottom: 60px;
`

const StatRow = styled(Row)`
    margin-bottom: 25px;
`

const Title = styled.h3`
    font-family: Nunito;
    font-weight: bold;
    font-size: 20px;
    line-height: 26px;
    text-align: center;
    letter-spacing: -0.2px;
    color: #1C1C1C;
    margin-bottom: 20px
`

const StyledBox = styled.div`
    padding: 22px 42px 42px;
    background: #FFEDD8;
    border-radius: 16px;
`

const ImgBox = styled.div`
    background: linear-gradient(180deg, #F0F0F0 0%, rgba(255, 255, 255, 0) 100%);
    border-radius: 16px;
    text-align: center;
    padding-top: 22px;
`

const Img = styled.img`
    width: 90%;
`

const Profession = () => {
    return (
        <Container className="container">
            <StatRow>
                <Col md={12} lg={6}>
                    <StyledBox>
                        <Title>
                            Sizdagi qiziqishlar
                        </Title>

                        {
                            interests.map((interest, index) => <InterestProgress key={index} {...interest} />)
                        }
                    </StyledBox>
                </Col>

                <Col md={12} lg={6}>
                    <StyledBox>
                        <Title>
                            Sizdagi qobiliyatlar
                        </Title>

                        <Chart />
                    </StyledBox>
                </Col>
            </StatRow>

            <ImgBox>
                <Title>
                    Sizga mos kasplarni aniqlaymiz
                </Title>

                <Img
                    src='/images/home_profession.png'
                    alt="Image error"
                />
            </ImgBox>
        </Container>
    );
}

export default Profession;