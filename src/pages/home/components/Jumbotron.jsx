import React from 'react';
import Image from "next/image";
import { Col, Row, Jumbotron as BootsJumbotron } from "react-bootstrap";
import styled from 'styled-components';
import Link from "next/Link";
import PrimaryBtn from "../../../commonComponents/PrimaryBtn";
import SecondaryBtn from "../../../commonComponents/SecondaryBtn";

const StyledJumbotron = styled(BootsJumbotron)`
    background-color: #00BBC7;
    height: 547px;
    padding: 0 100px;
    color: #ffffff;
    border-radius: 16px;
`
const StyledRow = styled(Row)`
    height: 100%;
`

const ContentCol = styled(Col)`
    display: flex;
    align-items: center;
`

const ImgCol = styled(Col)`
    display: flex;
    align-items: flex-end;
    padding-left: 80px;

    @media (max-width: 992px) {
        display: none;
    }
`

const Heading = styled.h1`
    font-family: Nunito;
    font-weight: 800;
    font-size: 42px;
    line-height: 50px;
    letter-spacing: -0.8px;
    margin-bottom: 15px;
`

const Description = styled.p`
    font-weight: 500;
    font-size: 18px;
    margin-bottom: 40px;
`
const StyledBtn = styled(PrimaryBtn)`
    margin-right: 7px;
`

const Jumbotron = () => {
    return (
        <StyledJumbotron className="container">
            <StyledRow>
                <ContentCol md={12} lg={7}>
                    <div>
                        <Heading>
                            Kelajakda o’zingizga munosib kasbni aniqlang
                        </Heading>

                        <Description>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                        </Description>

                        <Link href="/start-test">
                            <a>
                                <StyledBtn>
                                    Test topshir
                                </StyledBtn>
                            </a>
                        </Link>

                        <SecondaryBtn>
                            Kalkulyator
                        </SecondaryBtn>
                    </div>
                </ContentCol>

                <ImgCol lg={5}>
                    <Image
                        src="/images/jumbotron.png"
                        alt="Img error"
                        height={500}
                        width={330}
                    />
                </ImgCol>
            </StyledRow>
        </StyledJumbotron>
    );
}

export default Jumbotron;