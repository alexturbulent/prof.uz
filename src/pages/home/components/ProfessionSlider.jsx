import React from 'react';
import styled from "styled-components";
import SecondaryBtn from "../../../commonComponents/SecondaryBtn";
import Slider from "./Profession/Slider";

const ProfessionRow = styled.div`
    padding: 60px 0;
    background-color: #00BBC7;
    text-align: center;
`

const Title = styled.h3`
    text-align: center;
    color: #ffffff;
    margin-bottom: 40px;
`

const ProfessionSlider = () => {
    return (
        <ProfessionRow>
            <div className="container">
                <Title className="section-title">
                    Talab yuqori bo&apos;lgan kasblar
                </Title>

                <Slider />

                <SecondaryBtn>
                    Barcha kasblar
                </SecondaryBtn>
            </div>
        </ProfessionRow>
    );
}

export default ProfessionSlider;