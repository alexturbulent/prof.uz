import React from 'react';
import Image from "next/image";
import { Row, Col } from "react-bootstrap";
import styled from 'styled-components';

const FeaturedContainer = styled.div`
    margin-bottom: -80px;
`

const HeadingBox = styled.div`
    background-color: #FFA945;
    border-radius: 16px;
    height: 300px;
    padding: 40px;
    position: relative;

    h3 {
        color: #FFFFFF;
    }
`

const Message = styled.div`
    width: 181px;
    background: #FFE99A;
    border-radius: 20px;
    position: absolute;
    top: -65px;
    left: 15%;
    z-index: 10;
    padding: 20px;
    font-weight: 500;
    line-height: 19px;
    color: #965100;
`

const ImgRow = styled(Row)`
    position: relative;
    top: -130px;
    padding: 0 40px;
`

const list = [
    {
        message: "Men katta bo’lsam shifokor bo’laman",
        img: "/images/home_featured_1.png",
    },
    {
        message: "Men katta bo’lsam sportchi bo’laman",
        img: "/images/home_featured_2.png"
    },
    {
        message: "Men katta bo’lsam tikuvchi bo’laman",
        img: "/images/home_featured_3.png"
    },
    {
        message: "Men katta bo’lsam injener bo’laman",
        img: "/images/home_featured_4.png"
    },
]

const Featured = () => {
    return (
        <FeaturedContainer className="container">
            <HeadingBox>
                <h3 className="section-title"> 
                    Kelajakni hozirdan quring
                </h3>
            </HeadingBox>

            <ImgRow>
                {
                    list.map((item) => (
                        <Col key={item.img}>
                            <Message>
                                {item.message}
                            </Message>

                            <Image
                                src={item.img}
                                width={300}
                                height={300}
                                alt="Img error"
                            />
                        </Col>
                    ))
                }
            </ImgRow>
        </FeaturedContainer>
    );
}

export default Featured;