import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/globals.css';

import Navbar from "../commonComponents/Navbar";
import Footer from "../commonComponents/Footer";

function MyApp({ Component, pageProps }) {
    return (
        <>
            <Navbar />
            <Component {...pageProps} />
            <Footer />
        </>
    )
}

export default MyApp
