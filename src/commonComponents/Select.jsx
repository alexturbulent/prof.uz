import { Form } from "react-bootstrap";
import styled from "styled-components";
import 'bootstrap/dist/css/bootstrap.min.css';

const StyledSelect = styled.div`

`

const Select = (props) => {
    return (
        <StyledSelect>
            <Form.Control as="select" {...props}>
                {props.children}
            </Form.Control>
        </StyledSelect>
    );
}

export default Select;