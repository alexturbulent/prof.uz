import React from 'react';
import styled from 'styled-components';

const LabelContent = styled.p`
    margin-bottom: 20px;
    font-weight: 600;
    color: #FFA31A;

    span {
        background: #FFEDD8;
        border-radius: 33px;
        padding: 5px 20px;
    }
`

const Label = (props) => {
    return (
        <LabelContent>
            <span>
                {props.children}
            </span>
        </LabelContent>
    );
}

export default Label;