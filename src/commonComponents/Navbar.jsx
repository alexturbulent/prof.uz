import React from 'react';
import { Navbar, NavDropdown, Nav } from "react-bootstrap";
import Image from "next/image";
import Link from "next/link";
import styled from "styled-components";
import { menuList } from "../constants";

const StyledNavbar = styled(Navbar)`
    background-color: transparent !important;
    font-size: $fontSize;
    font-weight: 700;
`

const NavLink = styled(Nav.Link)`
    padding-left: 15px;
    padding-right: 15px;
    color: #000;
    display: flex;
    align-items: center;

    a {
        &:hover {
            text-decoration: none;
            color: #000 !important;
        }
    }
`

const Dropdown = styled(NavDropdown)`
    a {
        color: #000000 !important;
    }

    .email {
        font-size: 12px;
    }
`

const TopNavbar = () => {
    return (
        <StyledNavbar bg="light" expand="lg" className="container">
            <Navbar.Brand>
                <Link href='/'>
                    <a>
                        <Image
                            src='/images/logo.png'
                            width={150}
                            height={40}
                            alt="Image error"
                        />
                    </a>
                </Link>
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />

            <Navbar.Collapse className="justify-content-end">
                <Nav>
                    {
                        menuList.map((menu, index) => (
                            <NavLink key={index} as="span">
                                <Link href={menu.link}>
                                    <a>
                                        {menu.label}
                                    </a>
                                </Link>
                            </NavLink>
                        ))
                    }

                    <Dropdown title="Kirish">
                        <Link href='/'>
                            <NavDropdown.Item>
                                John Doe
                                <p className="email">example.com</p>
                            </NavDropdown.Item>
                        </Link>

                        <NavDropdown.Divider />

                        <Link href='/'>
                            <NavDropdown.Item>
                                Natijalar
                            </NavDropdown.Item>
                        </Link>

                        <Link href='/'>
                            <NavDropdown.Item>
                                Sozlamalar
                            </NavDropdown.Item>
                        </Link>

                        <Link href='/'>
                            <NavDropdown.Item>
                                Chiqish
                            </NavDropdown.Item>
                        </Link>
                    </Dropdown>
                </Nav>
            </Navbar.Collapse>
        </StyledNavbar>
    );
}

export default TopNavbar;