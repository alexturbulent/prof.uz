import styled from 'styled-components';
import PrimaryBtn from "./PrimaryBtn";

const SmallBtn = styled(PrimaryBtn)`
    font-size: 16px;
`

export default SmallBtn;