import { Button } from "react-bootstrap";
import styled from 'styled-components';

const PrimaryButton = styled(Button)`
    border-radius: 8px;
    font-size: 20px;
    padding: 10px 20px;
    background-color: #FFA31A;
    border-color: #FFA31A;
    transition: 300ms;
    
    &:hover {
        background-color: #FFA31A;
        border-color: #FFA31A;
        box-shadow: 0 0 10px 0px rgba(0,0,0,0.3);
    }
    
    &:active {
        background-color: #FFA31A !important;
        border-color: #FFA31A !important;
        box-shadow: 0 0 10px 0px rgba(0,0,0,0.3);
    }
    
    &:focus {
        background-color: #FFA31A !important;
        border-color: #FFA31A !important;
        box-shadow: 0 0 10px 0px rgba(0,0,0,0.3);
    }
`

export default PrimaryButton;