import styled from "styled-components";

const HeadingRow = styled.div`
    padding: 40px 0;
    background-color: #00BBC7;
    text-align: center;

    h3 {
        text-align: center;
        color: #ffffff;
    }
`

const PageHeading = (props) => {
    return (
        <HeadingRow>
            <div className="container">
                <h3 className="section-title">
                    {props.children}
                </h3>
            </div>
        </HeadingRow>
    );
}

export default PageHeading;