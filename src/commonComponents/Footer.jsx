import React from 'react';
import { Row, Col } from "react-bootstrap";
import { Youtube, Facebook, Instagram } from "react-bootstrap-icons";
import styled from "styled-components";
import Image from "next/image";
import Link from "next/link";

const Footer = styled.div`
    margin: 60px auto;
    background: #F4F4F4;
    border-radius: 16px;
    padding: 40px;
`

const CopyrightText = styled.p`
    color: #6E7381;
    margin-top: 5px;
`

const List = styled.ul`
    list-style: none;
    color: #1C1C1C;

    a:hover {
        color: #000000;
    }
`

const TopNavbar = () => {
    return (
        <div className="container">
            <Footer>
                <Row>
                    <Col lg={4}>
                        <Image
                            src='/images/logo.png'
                            width={150}
                            height={40}
                            alt="Image error"
                        />

                        <CopyrightText>
                            Xalq Ta’limi Vazirligi <br />Copyright 2021 (c) Multimedia markazi <br /> email: prof@itsm.uz
                        </CopyrightText>
                    </Col>

                    <Col>
                        <List>
                            <li>
                                <Link href="/">
                                    <a>Biz haqimizda</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Yordam</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Saytlarimiz</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Bog’lanish</a>
                                </Link>
                            </li>
                        </List>
                    </Col>

                    <Col>
                        <List>
                            <li>
                                <Link href="/">
                                    <a>Ro’yxatdan o’tish</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Tizimga kirish</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Kasb tanlash</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>Treninglar</a>
                                </Link>
                            </li>
                        </List>
                    </Col>

                    <Col>
                        <List>
                            <li>
                                <Link href="/">
                                    <a>xtv.uz</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>kitob.uz</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>ziyouz.com</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a>ziyonet.uz</a>
                                </Link>
                            </li>
                        </List>
                    </Col>

                    <Col>
                        <List>
                            <li>
                                <Link href="/">
                                    <a><Youtube /> /youtube</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a><Facebook /> /facebook</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/">
                                    <a><Instagram /> /instagram</a>
                                </Link>
                            </li>
                        </List>
                    </Col>
                </Row>
            </Footer>
        </div>
    );
}

export default TopNavbar;