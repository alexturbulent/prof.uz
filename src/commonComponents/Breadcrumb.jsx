import styled from "styled-components";
import Link from "next/link";

const HeadingRow = styled.div`
    padding: 18px 0;
    background-color: #00BBC7;
`

const Breadcrumb = styled.div`
    display: flex;
    opacity: 0.7;
    color: #FFFFFF;

    a {
        opacity: inherit;
        color: #FFFFFF;
    }

    .slash {
        margin: 0 10px;
    }
`

const StyledBreadcrumb = (props) => {
    const list = props.list;

    if (list && Array.isArray(list)) {
        return (
            <HeadingRow>
                <div className="container">
                    <Breadcrumb>
                        {
                            list.map((item, index) => (
                                <span key={item.path}>
                                    <Link
                                        href={item.path}
                                    >
                                        <a>
                                            {item.label}
                                        </a>
                                    </Link>
                                    {
                                        index + 1 !== list.length && (
                                            <span className="slash">/</span>
                                        )
                                    }
                                </span>
                            ))
                        }
                    </Breadcrumb>
                </div>
            </HeadingRow>
        );
    }

    return null;
}

export default StyledBreadcrumb;