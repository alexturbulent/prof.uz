import styled from 'styled-components';
import { Button } from "react-bootstrap";

const SecondaryBtn = styled(Button)`
    background-color: #ffffff;
    border-color: #ffffff;
    color: #48CCD4;
    border-radius: 8px;
    font-size: 20px;
    padding: 10px 20px;
    transition: 300ms;
    
    &:hover {
        background-color: #ffffff;
        border-color: #ffffff;
        color: #48CCD4;
    }
    
    &:active {
        background-color: #ffffff !important;
        border-color: #ffffff !important;
        color: #48CCD4 !important;
    }
    
    &:focus {
        background-color: #ffffff !important;
        border-color: #ffffff !important;
        color: #48CCD4 !important;
    }
`

export default SecondaryBtn;