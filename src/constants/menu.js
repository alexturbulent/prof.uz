export const menuList = [
    {
        label: 'Kasb tanlash',
        link: '/professions'
    },
    {
        label: 'Rezume yarat',
        link: '/'
    },
    {
        label: 'Treninglar',
        link: '/'
    },
    {
        label: "Biz bilan bog'laning",
        link: '/contact-us'
    },
    {
        label: 'Biz haqimizda',
        link: '/about-us'
    },
]